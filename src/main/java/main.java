import com.filenet.api.collection.RepositoryRowSet;
import com.filenet.api.core.*;
import com.filenet.api.query.RepositoryRow;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.UserContext;
import filenet.vw.api.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.security.auth.Subject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class main
{
    public static Logger logger = LoggerFactory.getLogger(main.class);
    public static Properties prop = new Properties();
    public static VWSession vwSession;
    public static VWRoster roster;
    public static InputStream pfile;
    public static int queryType;
    public static VWRosterQuery query;
    public static int queryFlags;
    public static ObjectStore os;
    public static Calendar cal;
    public static int Proccount;
    public static int Doccount;
    public static int Packcount;
    public static SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public static Date date;
    public static Date currentDate;
    public static File file;

    public static void main(String[] args) {
        logger.info("started");
        logger.trace("loading properties");
        pfile = main.class.getClassLoader().getResourceAsStream("config.prop");
        try {
            prop.load(pfile);
        } catch (IOException e) {
            logger.error("in exception");
            e.printStackTrace();
        }
        logger.debug("loading server");
        String server = prop.getProperty("server");
        logger.trace(server);
        String port = prop.getProperty("port");
        logger.trace(port);
        String username = prop.getProperty("username");
        logger.trace(username);
        String password = prop.getProperty("password");
        logger.trace("" + password.length());
        String cp = prop.getProperty("cp");
        logger.trace(cp);
        String obstore = prop.getProperty("os");
        logger.trace(obstore);
        Connection con = Factory.Connection.getConnection("http://" + server + ":" + port + "/wsi/FNCEWS40MTOM");
        logger.debug(con.getURI());
        Subject subject = UserContext.createSubject(con, username, password, "FileNetP8WSI");
        UserContext uc = UserContext.get();
        uc.pushSubject(subject);
        Domain domain = Factory.Domain.fetchInstance(con,null,null);
        os = (ObjectStore) domain.fetchObject("ObjectStore",obstore,null);
        logger.trace(domain.get_Name());
        logger.trace(os.get_SymbolicName());
        currentDate = new Date();
        logger.info("Current time: "+currentDate.toString());
        date = new Date(System.currentTimeMillis() - (1 * 60 * 60 * 1000));
        logger.info("Finding date:"+date.toString());
        cal = Calendar.getInstance();
        cal.setTime(date);
        Proccount=0;
        try {
            logger.trace("tryin to login");
            vwSession = new VWSession(username, password, cp);
            vwSession.logon(username, password, cp);
            logger.trace("logged");
            roster = vwSession.getRoster("DefaultRoster");
            logger.trace("reosterfetched");
            logger.debug("Total process started: " + roster.fetchCount());
            String queryFilter = "F_StartTime>="+date.getTime() / 1000;
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            queryType = VWFetchType.FETCH_TYPE_WORKOBJECT;
            queryFlags = VWRoster.QUERY_NO_OPTIONS;
            query = roster.createQuery(null, null, null, queryFlags, queryFilter, null, queryType);
            logger.info("Workflow started on last hour: " + query.fetchCount());
            Proccount=query.fetchCount();
            vwSession.logoff();
        } catch (VWException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
        try{
            String packSql = "SELECT Name,Id,DocNum FROM DocumentsPack WHERE DateCreated > "+formatter.format(cal.getTime());
            logger.trace(packSql);
            logger.trace(os.get_SymbolicName());
            SearchSQL sqlObject = new SearchSQL(packSql);
            SearchScope searchScope = new SearchScope(os);
            RepositoryRowSet rowSet = searchScope.fetchRows(sqlObject, null, null, new Boolean(true));
            Iterator iterator =rowSet.iterator();
            Packcount=0;
            while (iterator.hasNext()){
                RepositoryRow repositoryRow = (RepositoryRow)iterator.next();
                logger.trace(repositoryRow.getProperties().getStringValue("Name")+repositoryRow.getProperties().getStringValue("DocNum"));
                Packcount++;
            }
            logger.info("Packages Created on last hour: "+Packcount);

        }catch (Exception e){
            e.printStackTrace();
        }
        try{

            String packSql = "SELECT Name,Id FROM SZMNDocuments WHERE DateCreated > "+formatter.format(cal.getTime())+" AND IsCurrentVersion=TRUE";
            SearchSQL sqlObject = new SearchSQL(packSql);
            logger.trace(os.get_SymbolicName());
            SearchScope searchScope = new SearchScope(os);
            RepositoryRowSet rowSet = searchScope.fetchRows(sqlObject, null, null, new Boolean(true));
            Iterator iterator =rowSet.iterator();
            Doccount=0;
            while (iterator.hasNext()){
                RepositoryRow repositoryRow = (RepositoryRow)iterator.next();
                logger.trace(repositoryRow.getProperties().getStringValue("Name"));
                Doccount++;
            }
            logger.info("Documtnts Created on last hour: "+Doccount);

        }catch (Exception e){
            e.printStackTrace();
        }
        file = new File("out.xls");
        if (file.exists()) {
            try {
                HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(file.getName()));
                    Sheet sheet = workbook.getSheetAt(workbook.getNumberOfSheets()-1);
                    if (sheet.getSheetName().equalsIgnoreCase(new SimpleDateFormat("dd.MM.yyyy").format(cal.getTime()))){
                        logger.trace("sheet found add row");
                        Row row = sheet.createRow(sheet.getLastRowNum()+1);
                        Cell c1 = row.createCell(0);
                        Cell c2 = row.createCell(1);
                        Cell c3 = row.createCell(2);
                        Cell c4 = row.createCell(3);
                        c1.setCellValue(new SimpleDateFormat("HH:mm:ss").format(cal.getTime()));
                        c2.setCellValue(Proccount);
                        c3.setCellValue(Doccount);
                        c4.setCellValue(Packcount);
                        FileOutputStream out = new FileOutputStream(file);
                        workbook.write(out);
                        out.close();
                        }else{
                        Sheet sheeet = workbook.createSheet(new SimpleDateFormat("dd.MM.yyyy").format(cal.getTime()));
                        Row roow = sheeet.createRow(0);
                        roow.createCell(0).setCellValue("Время запуска");
                        roow.createCell(1).setCellValue("Количество процессов");
                        roow.createCell(2).setCellValue("Количество документов");
                        roow.createCell(2).setCellValue("Количество пакетов");
                        Row roow2 = sheeet.createRow(1);
                        roow2.createCell(0).setCellValue(new SimpleDateFormat("HH:mm:ss").format(cal.getTime()));
                        roow2.createCell(1).setCellValue(Proccount);
                        roow2.createCell(2).setCellValue(Doccount);
                        roow2.createCell(3).setCellValue(Packcount);
                        FileOutputStream out = new FileOutputStream(file);
                        workbook.write(out);
                        out.close();
                    }
                    }catch (Exception e){
                e.printStackTrace();
                logger.error(e.getLocalizedMessage());
                }
            }else{
            logger.error("CreateFile");
        }
        }
}