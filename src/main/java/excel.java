import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class excel{
    private static HSSFWorkbook workbook=null;
    private static Sheet sheet=null;
    private static Row row=null;
    private static Logger logger = LoggerFactory.getLogger(excel.class);


    public static void createStatSheet(String filename){
        try {
            workbook = new HSSFWorkbook(new FileInputStream(filename));
            Sheet sheet = workbook.createSheet(new SimpleDateFormat("dd.MM.yyyy").format(new Date().getTime())+"_DocStat");
            logger.trace(sheet.getSheetName());
            Row roow = sheet.createRow(0);
            roow.createCell(0).setCellValue("Время запуска");
            roow.createCell(1).setCellValue("Количество процессов");
            roow.createCell(2).setCellValue("Количество документов");
            roow.createCell(2).setCellValue("Количество пакетов");
            workbook.write(new FileOutputStream(filename));
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }

    public static void createDocStatSheet(File file){
        try {
            HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(file.getName()));
            Sheet sheet = workbook.createSheet(new SimpleDateFormat("dd.MM.yyyy").format(new Date().getTime()));
            Row roow = sheet.createRow(0);
            roow.createCell(0).setCellValue("Тип документа");
            roow.createCell(1).setCellValue("Количество");
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }

    public static boolean checkSheet(File file,String sheetName){
        Boolean sheetExist = Boolean.FALSE;
        try {
            HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(file.getName()));
            int sheetCount = workbook.getNumberOfSheets();
            for(int sheetN=0;sheetN<sheetCount;sheetN++){
                if (workbook.getSheetAt(sheetN).getSheetName().equalsIgnoreCase(sheetName)){
                    sheetExist=Boolean.TRUE;
                    logger.trace("sheet with name "+sheetName+" found. Returning "+sheetExist.toString());
                }else {
                    logger.trace("SheetName "+workbook.getSheetAt(sheetN).getSheetName()+" not equals to "+sheetName);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
        logger.info("returning "+sheetExist.toString());
        return sheetExist;
    }

    public static Sheet getSheet(File file,String sheetName){
        try{
            HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(file.getName()));
            sheet = workbook.getSheet(sheetName);
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
        return sheet;
    }

    public static Row getLastRow(File file,String sheetName){
        try{
            workbook = new HSSFWorkbook(new FileInputStream(file.getName()));
            logger.trace(workbook.toString());
            Sheet sheet = workbook.getSheet(sheetName);
            logger.trace(sheet.getSheetName());
            row = sheet.getRow(sheet.getLastRowNum()+1);
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
        return row;
    }

    public static void writeRowStat(File file,String Proccount,String Doccount,String Packcount){
        try{
            Cell c1 = row.createCell(0);
            Cell c2 = row.createCell(1);
            Cell c3 = row.createCell(2);
            Cell c4 = row.createCell(3);
            c1.setCellValue(new SimpleDateFormat("HH:mm:ss").format(new Date().getTime()));
            c2.setCellValue(Proccount);
            c3.setCellValue(Doccount);
            c4.setCellValue(Packcount);
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();
        }catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }

    public static void writeRowDocStat(String Doctype,int count){
        try{
            Cell c1 = row.createCell(0);
            Cell c2 = row.createCell(1);
            c1.setCellValue(Doctype);
            c2.setCellValue(count);
        }catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }

    public static void main(String[]args) {

    }
}